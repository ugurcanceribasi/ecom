<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/products/{category_id}', [App\Http\Controllers\HomeController::class, 'products'])->name('products');
Route::get('/product_details/{product_id}', [App\Http\Controllers\HomeController::class, 'product_details'])->name('product.details');
Route::get('/cart', [\App\Http\Controllers\HomeController::class, 'cart'])->name('cart');
Route::get('/payment', [\App\Http\Controllers\HomeController::class, 'payment'])->name('payment');
Route::get('/payment/success', [\App\Http\Controllers\HomeController::class, 'payment_success'])->name('payment_success');


Route::post('/address/create', [\App\Http\Controllers\AddressController::class, 'store'])->name('address');
Route::post('/order/create', [\App\Http\Controllers\OrderController::class, 'store'])->name('order');
Route::post('/add_to_cart', [\App\Http\Controllers\CartController::class, 'addToCart'])->name('addtocart');
Route::post('/remove_to_cart', [\App\Http\Controllers\CartController::class, 'removeToCart'])->name('removetocart');


Route::redirect('/admin', '/admin/categories');
Route::group(['prefix' => 'admin', 'middleware' => 'isadmin'], function () {
    Route::resource('categories', \App\Http\Controllers\CategoryController::class);
    Route::resource('products', \App\Http\Controllers\ProductController::class);
    Route::resource('orders', \App\Http\Controllers\OrderController::class);
    Route::post('orders/changestatus', [ \App\Http\Controllers\OrderController::class, 'changeStatus']);
});
