<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddressRequest;
use App\Models\Address;

class AddressController extends Controller
{

    public function store(AddressRequest $request)
    {
        $address = $request->all();
        Address::create($address);
        return redirect()->back();
    }

}
