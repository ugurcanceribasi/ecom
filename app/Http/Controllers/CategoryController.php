<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return Inertia::render('admin/categories/index', ['categories' => $categories]);
    }


    public function store(CategoryRequest $request)
    {
        if($request->hasFile('category_image'))
        {
            if($request->file('category_image')->isValid())
            {
                $extension = $request->category_image->extension();
                $request->category_image->storeAs('/public',$request->category_name.'.'.$extension);
                $url = Storage::url($request->category_name.".".$extension);
                $datas = $request->all();
                $datas['category_image'] = $url;
                Category::create($datas);
                return redirect()->back();
            }
        }
    }

    public function update(Request $request)
    {
        if($request->has('id'))
        {
            if($request->hasFile('category_image')) {
                if ($request->file('category_image')->isValid()) {
                    $extension = $request->category_image->extension();
                    $request->category_image->storeAs('/public',$request->category_name.'.'.$extension);
                    $url = Storage::url($request->category_name.".".$extension);
                    $datas = $request->all();
                    $datas['category_image'] = $url;
                    Category::find($request->get('id'))->update($datas);
                    return redirect()->back();
                }
            }
            else{
                Category::find($request->get('id'))->update($request->all());
                return redirect()->back();
            }

        }
    }

    public function destroy(Request $request)
    {
        if($request->has('id'))
        {
            Category::find($request->get('id'))->delete();
            return redirect()->back();
        }
    }
}
