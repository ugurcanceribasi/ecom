<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function addToCart(CartRequest $request)
    {

        $cart = session()->get('cart');
        $product = $request->all();

        if(!$cart)
        {
            $this->defineCart($product);
        }

        if(isset($cart[$product['product_id']]))
        {
            $this->updateProductDetails($cart, $product);
        }

        $cart[$product['product_id']] = $product;
        session()->put('cart', $cart);
        return response()->json([
            'status' => 200,
            'cart' => $cart,
            'message' => 'Ürün sepetinize eklendi!'
        ]);
    }

    public function removeToCart(Request $request)
    {
        $product_id = $request->get("product_id");
        $cart = session()->get('cart');
        if(isset($cart[$product_id]))
        {
            unset($cart[$product_id]);
            session()->put('cart', $cart);
            return response()->json([
                "status" => 200,
                "cart" => $cart,
                "message" => "Sepetinizden kaldırıldı!"
            ]);
        }

        return response()->json([
            "status" => 200,
            "product_id" => $product_id,
            "message" => "Sepetinizde yok!"
        ]);
    }

    public function defineCart($product)
    {
        $cart = [
            $product['product_id'] => $product
        ];
        session()->put('cart', $cart);
        return response()->json([
            'status' => 200,
            'cart' => $cart,
            'message' => 'Ürün sepetinize eklendi!'
        ]);
    }

    public function updateProductDetails($cart, $product)
    {
        $product_id = $product['product_id'];
        $cart[$product_id]['quantity'] += $product['quantity'];
        $cart[$product_id]['total_price'] = $cart[$product_id]['quantity'] * $cart[$product_id]['price'];
        session()->put('cart', $cart);
        return response()->json([
            'status' => 200,
            'cart' => $cart,
            'message' => 'Ürün sepetinizde güncellendi!'
        ]);
    }
}
