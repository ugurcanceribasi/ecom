<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class ProductController extends Controller
{

    public function index()
    {

        $products = Product::orderBy('id','desc')->get()->map(function ($product) {
            return [
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description,
                'image' => $product->image,
                'price' => $product->price,
                'brand' => $product->brand,
                'category_id' => $product->category_id,
                'category' => $product->category->category_name
            ];
        });

        $categories = Category::all();

        return Inertia::render('admin/products/index', ['products' => $products, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(ProductRequest $request)
    {
        if($request->hasFile('image'))
        {
            if($request->file('image')->isValid())
            {
                $extension = $request->image->extension();
                $request->image->storeAs('/public',$request->name.'.'.$extension);
                $url = Storage::url($request->name.".".$extension);
                $datas = $request->all();
                $datas['image'] = $url;
                Product::create($datas);
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    public function update(Request $request)
    {
        if($request->has('id'))
        {
            if($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $extension = $request->image->extension();
                    $request->image->storeAs('/public',$request->name.'.'.$extension);
                    $url = Storage::url($request->name.".".$extension);
                    $datas = $request->all();
                    $datas['image'] = $url;
                    Product::find($request->get('id'))->update($datas);
                    return redirect()->back();
                }
            }
            else{
                Product::find($request->get('id'))->update($request->all());
                return redirect()->back();
            }

        }
    }

    public function destroy(Request $request)
    {
        if($request->has('id'))
        {
            Product::find($request->get('id'))->delete();
            return redirect()->back();
        }
    }
}
