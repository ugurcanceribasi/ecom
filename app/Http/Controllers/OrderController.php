<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatus;
use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Inertia\Inertia;

class OrderController extends Controller
{

    public function index()
    {
        $orders = Order::orderBy('id','desc')->get()->map(function ($order) {
            return [
                'order_id' => $order->id,
                'user' => $order->user,
                'address' => $order->address,
                'amount' => $order->amount,
                'status' => OrderStatus::getStatus($order->status),
                'details' => $order->details->map(function($order_details){
                    return [
                        'order_id' => $order_details->order_id,
                        'product_id' => $order_details->product_id,
                        'price' => $order_details->price,
                        'quantity' => $order_details->quantity,
                        'product' => $order_details->product
                    ];
                }),
                'date' => date('d.m.Y H:i',strtotime($order->created_at)),
            ];
        });
        return Inertia::render('admin/orders/index', ['orders' => $orders]);
    }


    public function store(OrderRequest $request)
    {
        $order = $request->all();
        $createdOrder = Order::create($order);
        $cart = session()->get('cart');
        foreach ($cart as $key => $value)
        {
            $order_details = [];
            $order_details['order_id'] = $createdOrder->id;
            $order_details['product_id'] = $key;
            $order_details['price'] = $value['price'];
            $order_details['quantity'] = $value['quantity'];
            OrderDetail::create($order_details);
        }
        return redirect('/payment/success')->with('order_id', $createdOrder->id);
    }


    public function changeStatus(Request $request)
    {
        $order_id = $request->get('order_id');
        $status = $request->get('status');
        Order::find($order_id)->update([
            'status' => $status
        ]);
        return redirect()->back();
    }

}
