<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use MongoDB\Driver\Session;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $categories = Category::all();
        return Inertia::render('home/index', ['categories' => $categories]);
    }

    public function products($category_id)
    {
        $products = Product::where('category_id', $category_id)->get();
        return Inertia::render('home/products/index', ['products' => $products]);
    }

    public function product_details($product_id)
    {
        $product = Product::find($product_id);
        return Inertia::render('home/products/detail', ['product' => $product]);
    }

    public function cart()
    {
        $cart = session()->get('cart');
        if(!$cart)
        {
            return redirect('/');
        }
        return Inertia::render('home/cart/index', ['cart' => $cart]);
    }

    public function payment()
    {
        $user = auth()->user();
        $addresses = Address::where('user_id', $user->id)->get();
        $cart = session()->get('cart');
        if(empty($cart))
        {
            return redirect('/');
        }
        return Inertia::render('home/payment/index', ['cart' => $cart, 'user' => $user, 'addresses' => $addresses]);
    }

    public function payment_success()
    {
        $order_id = session()->get('order_id');
        session()->remove('cart');
        return Inertia::render('home/payment/success', ['title' => 'Siparişiniz alındı!', 'message' => 'Siparişinizi sipariş takip sayfasından takip edebilirsiniz.', 'order_id' => $order_id]);
    }

}
