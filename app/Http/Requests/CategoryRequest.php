<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_name'        => 'required|min:3|max:80',
            'category_image' => 'required|image',
            'category_description' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'category_name' => 'Kategori Adı',
            'category_description' => 'Açıklama',
            'category_image'       => 'Resim',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute alanı boş geçilemez!',
            'min' => ':attribute alanı en az :min karakterden oluşmalıdır!',
            'max' => ':attribute alanı en fazla :max karakterden oluşmalıdır!'
        ];
    }
}
