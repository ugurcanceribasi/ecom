<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|min:3|max:80',
            'image' => 'required|image',
            'description' => 'required',
            'price' => 'required|numeric',
            'brand' => 'required|min:3|max:80',
            'category_id' => 'required|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Ürün Adı',
            'description' => 'Açıklama',
            'image'       => 'Resim',
            'price' => 'Fiyat',
            'brand' => 'Marka',
            'category_id' => "Kategori"
        ];
    }
}
