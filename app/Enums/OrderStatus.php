<?php

namespace App\Enums;

final class OrderStatus
{
    const WAITING_FOR_APPROVAL = 0;
    const APPROVED = 1;
    const SHIPPED = 2;
    const DELIVERED = 3;

    public static function toArray()
    {
        return [
            self::WAITING_FOR_APPROVAL,
            self::APPROVED,
            self::SHIPPED,
            self::DELIVERED
        ];
    }

    public static function getStatus($status)
    {
        switch ($status){
            case self::WAITING_FOR_APPROVAL:
                return 'Onay Bekliyor';
            case self::APPROVED:
                return 'Onaylandı';
            case self::SHIPPED:
                return 'Kargoya Verildi';
            case self::DELIVERED:
                return "Teslim Edildi";
            default:
                return "Bilinmiyor";
        }
    }

    public static function getDefaultStatus()
    {
        return self::WAITING_FOR_APPROVAL;
    }

}
