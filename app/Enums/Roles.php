<?php

namespace App\Enums;

final class Roles
{
    const ROLE_ADMIN = "admin";
    const ROLE_CUSTOMER = "customer";

    public static function toArray()
    {
        return [self::ROLE_ADMIN, self::ROLE_CUSTOMER];
    }

    public static function getDefaultRole()
    {
        return self::ROLE_CUSTOMER;
    }

}
